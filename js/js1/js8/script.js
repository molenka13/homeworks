//DOM - це об'єктна модель документа , яка відображає контент сторінки у вигляді об'єктів які можна змінювати
//innerText встановлює контент тега у вигляді простого тексту , а innerHTML встановлює контент у форматі HTML
//можна звернутися до елементу сторінки за допомогою .querySelector(.querySelectorAll) , також за допомогою getElementBy...(getElementsBy...).Самий універсальний і легший спосіб дотягнутися до елементу через .querySelector() або querySelectorAll()
let findAllParagr = document.getElementsByTagName('p')
for(let i = 0;i < findAllParagr.length;i++){
findAllParagr[i].style.backgroundColor = '#ff0000'
}
 
let optionsList = document.getElementById('optionsList')
console.log(optionsList);
let parentsOptionsList = optionsList.parentNode
console.log(parentsOptionsList);
let childrenOptionsList = optionsList.childNodes
for(let child of childrenOptionsList){
    console.log(child.className)
    console.log(child.nodeType);
}

let testParagraph = document.getElementById('testParagraph')
testParagraph.innerText  = 'This is a paragraph'

let mainHeader = document.getElementsByClassName('main-header')
let childrenMainHeader = mainHeader.childNodes
let classMainHeader = document.body.classList.add('nav-item')
console.log(childrenMainHeader);

let allSectionTitle = document.getElementsByClassName('section-title')
document.body.classList.remove('section-title')