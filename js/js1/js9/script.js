//новый HTML тег на сторінці можна створити за допомогою document.createElement(тег) і потім додати його на сторінку за допомогою за допомогою document.body.append(тег)(або prepend,before,after)
//перший параметр insertAdjacentHTML означає куди вставляти елемент .всього існує 4 варіанти :beforestart , afterstart , beforeend ,afterend.
//щоб видалити елемент потрібно використати node.remove()
let getList = (arr , doc = document.body) => {
    let ul = document.createElement('ul')
    doc.append(ul)
    arr.forEach(element => {
        ul.insertAdjacentHTML('beforeend' , `<li>${element}</li>`)
    });
}
getList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])
