const password = document.querySelector('.first-password')
const confirmPassword = document.querySelector('.confirm-password')
const btn = document.querySelector('.btn')
const faEye = document.querySelectorAll('.fa-eye')
const faEyeSlash = document.querySelectorAll('.fa-eye-slash')
const faEyeFir = document.querySelector('.fa-eye-fir')
const faEyeSlashFir = document.querySelector('.fa-eye-slash-fir')
const faEyeSec = document.querySelector('.fa-eye-sec')
const faEyeSlashSec = document.querySelector('.fa-eye-slash-sec')
const firstInput = document.querySelector('.first-input')
faEye.forEach(element => {
    element.style.display = 'none'
});
document.querySelectorAll('.fir-icon').forEach(element => {
    element.addEventListener('click' , () => {
        if(password.getAttribute('type') == 'password'){
            faEyeFir.style.display = 'inline-block'
            faEyeSlashFir.style.display = 'none'
            password.setAttribute('type' , 'text')
        }else{
            faEyeFir.style.display = 'none'
            faEyeSlashFir.style.display = 'inline-block'
            password.setAttribute('type' , 'password')
        }
    })
});


document.querySelectorAll('.sec-icon').forEach(element => {
    element.addEventListener('click' , () => {
        if(confirmPassword.getAttribute('type') == 'password'){
            faEyeSec.style.display = 'inline-block'
            faEyeSlashSec.style.display = 'none'
            confirmPassword.setAttribute('type' , 'text')
        }else{
            faEyeSec.style.display = 'none'
            faEyeSlashSec.style.display = 'inline-block'
            confirmPassword.setAttribute('type' , 'password')
        }
    })
});


let inequalPassword = document.createElement('h3')
inequalPassword.innerText = 'Потрібно ввести однакові значення'
inequalPassword.style.color = 'red'
inequalPassword.style.display = 'none'
firstInput.append(inequalPassword)
btn.addEventListener('click' , () => {
    btn.setAttribute('type' , 'button')
    if(password.value === confirmPassword.value){
        alert('you are welcome')
        if(inequalPassword.style.display === 'inline'){
            inequalPassword.style.display = 'none'
        }
    }else{
        inequalPassword.style.display = 'inline'
    }
})





