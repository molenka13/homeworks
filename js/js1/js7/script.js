//forEach перебирає кожен елемент масиву і запускає для нього функцію
//щоб очистити масив можна задати length значення 0.наприклад: array.length = 0
//можна використати метод isArray() щоб дізнатися чи є змінна масивом
const filterBy = (array, type) => array.filter(item => typeof item !== type)

console.log(filterBy( ['hello', 'world', 23, '23', null],'string'))
