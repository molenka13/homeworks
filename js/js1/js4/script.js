//коли є якийсь шматок коду , який потрібно використовувати декілька разів можна використовувати функції
//передавати параметри потрібно для обробки їх всередині функції.наприклад у вас є функція додавання двох чисел і вам потрібно щоб саме два конкретних числа додавалися , тому вам потрібно передати їх в функцію , як аргументи.
//return - це оператор який працює всередині функції і повертає нам якесь значення .також  , цей оператор зупиняє роботу функції.
let mathOperation = () =>{
    let firstNum = Number(prompt('enter first number '))
    let secondNum = Number(prompt('enter second number '))
    let mathSign = prompt('enter math sign')
    if(mathSign === '+'){
        console.log(firstNum + secondNum);
    }else if(mathSign === '-'){
        console.log(firstNum - secondNum);
    }else if(mathSign === '*'){
        console.log(firstNum * secondNum);
    }else if(mathSign === '/'){
        console.log(firstNum / secondNum);
    }
}
mathOperation()
